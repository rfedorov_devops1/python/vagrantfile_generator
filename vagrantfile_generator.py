import yaml, os
from jinja2 import Template

class ReadFile:
    def __init__(self):
        self.__filename = str()
    
    def file_reader(self, filename):
        self.__filename = filename
        try:
            with open(self.__filename, 'r') as self.__file:
                return self.__file.read()
        except IOError:
            print(f'Error while read {filename}', filename=self.__filename)
            os._exit(1)

class TemplatingFile:
    def __init__(self):
        self.__template = str()
        self.__vars_file = str()
    
    def templating(self, template, vars_file):
        self.__template = template
        self.__vars_file = vars_file
        self.__templated_file = Template(self.__template).render(dict=yaml.safe_load(self.__vars_file))
        return self.__templated_file

class WriteFile(ReadFile, TemplatingFile):
    def __init__(self):
        ReadFile.__init__(self)
        TemplatingFile.__init__(self)

    def file_writer(self, vars_file, template, result_file):
        self.__vars_file = vars_file
        self.__template = template
        self.__result_file = result_file

        try:
            with open(self.__result_file, 'w') as self.__file:
                self.__file.write(self.templating(vars_file=self.file_reader(self.__vars_file),
                                            template=self.file_reader(self.__template)))
        except IOError:
            print(f'Error while write to file {filename}', filename=self.__result_file)
            os._exit(1)

def main():
    generate_file = WriteFile()
    generate_file.file_writer(vars_file='variables.yml', template='Vagrantfile.j2', result_file='Vagrantfile')

if __name__ == '__main__':
    main()
